const bcrypt = require('bcryptjs/dist/bcrypt')
const db = require('../../config/database')

const index = async (req, res) => {
    try { 
        const page = req.query.page ? req.query.page : 1;

        const result = await db('users')
            .select([
                'users.id',
                'users.name',
                'users.email',
                'users.created_at',
                'users.updated_at',
            ])
            .paginate({ perPage: 2, currentPage: parseInt(page), isLengthAware: true })

        result.message = "OK"

        res.status(200).json(result)
    } catch (error) {
        console.error(error);
        res.status(500).json({
            message: "Internal Server Error!"
        })
    }
}

const store = async (req, res) => {
    try {
        let encryptedPassword = await bcrypt.hash(req.body.password, 10);

        const result = await db('users')
            .insert({
                name: req.body.name,
                email: req.body.email,
                password: encryptedPassword,
            }, [
                'id',
                'name',
                'email',
            ])

        res.status(200).json({
            message: "OK",
            data: result[0]
        })
    } catch (error) {
        console.error(error);
        res.status(500).json({
            message: "Internal Server Error!"
        })
    }
}

const show = async (req, res) => {
    try {
        const result = await db('users')
            .select([
                'users.id',
                'users.name',
                'users.email',
                'users.created_at',
                'users.updated_at',
            ])
            .where('id', '=', req.params.id)
            .first()

        res.status(200).json({
            message: "OK",
            data: result
        })
    } catch (error) {
        console.error(error);
        res.status(500).json({
            message: "Internal Server Error!"
        })
    }
}

const update = async (req, res) => {
    const result = await db('users')
    res.status(200).json(result)
}

const destroy = async (req, res) => {
    const result = await db('users')
    res.status(200).json(result)
}

module.exports = {
    index,
    store,
    show,
    update,
    destroy,
}