require('dotenv').config()

const bodyParser = require('body-parser')

var express = require('express')
var app = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const authRouter = require('./routes/auth')
const userRouter = require('./routes/user')

app.use('/api', userRouter)
app.use('/api', authRouter)

app.listen(process.env.APP_PORT, () => {
    console.log(`listening on port ${process.env.APP_PORT}!`)
})
