const express = require('express')

const router = express.Router()

const UserController = require('../app/controller/user.controller')
const UserValidator = require('../app/validator/user.validator')
const AuthMiddleware = require('../middleware/auth.middleware')
 
router.get('/user', AuthMiddleware, UserController.index)
router.get('/user/:id', AuthMiddleware, UserController.show)
router.post('/user', AuthMiddleware, UserValidator.insert, UserController.store)

module.exports = router